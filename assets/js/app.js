$(document).ready( function() {
	// upload form hack that let's me style it
	$("#upload-input").change(function() {
		$("#caption").css('display', 'inherit');
		//$("#file").addClass("notification");
		$("#file").text($("#upload-input").val().replace(/C:\\fakepath\\/i, ''));
	});

	$("#browse-button").click(function(){
	   $("#upload-input").trigger('click');
	   return false;
	});

	// ajax to follow ppl
	$('#follow-button').click(function() {
		$.ajax({
			url: "app/f/follow_user.php",
			type: "POST",
			data: "u=" + $('#victim').val(),
			cache: false,
			success: function(response) {
				if(response == "true") {

					$('#follow-button').removeClass('notfollowing').addClass('following');
					$('#follow-button').html('Following');
				}
			}
		});
	});
});