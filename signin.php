<?php require_once("template/header.php") ?>

<?php if( Session::isUserSignedIn() ) header('Location: user.php') ?>

<div id="navigation">
	<div id="navigation-inner" class="container">
		<div id="logo"><a href="http://localhost/tweb2012/">Phototo</a></div>
		<div class="right">
			<?php include('template/inner_navigation.php') ?>
		</div>

		<div class="clear"></div>
	</div>
</div>

<div id="container">
	<?php if( isset($_GET['m']) && $_GET['m'] == 'error') : ?>
		<div class="error">
			We can't sign you in.
		</div>
	<?php endif; ?>

	<div id="content">
		<h2>Sign in with your account</h2>
		<form action="app/f/sign_in_user.php" id="signin" method="post">
			<p>
				<label for="username">Username</label>
				<input type="text" name="username" class="input">
			</p>
			<p>
				<label for="password">Password</label>
				<input type="password" name="password" class="input">
			</p>
			<p>
				<input type="submit" name="signin" value="Sign In" class="button">
			</p>
		</form>
	</div>
</div>

<?php require_once("template/footer.php") ?>