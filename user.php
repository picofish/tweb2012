<?php require_once("template/header.php") ?>

<?php require_once("app/config.php") ?>
<?php require_once("app/lib/Post.php") ?>
<?php require_once("app/lib/User.php") ?>
<?php require_once("app/lib/Session.php") ?>

<?php
	$canDisplayUpload = false;
	$canDisplayFollow = false;
	if(Session::isUserSignedIn()) {
		if(isset($_GET['u']) && $_GET['u'] != "") {
			// viewing other user, i'm logged in
			$user = $_GET['u'];

			$canDisplayFollow = true;
		} else {
			$user = Session::signedInUser();
			$canDisplayUpload = true;
		}
	} else {
		if(isset($_GET['u']) && $_GET['u'] != "") {
			// viewing other user, i'm NOT logged in
			$user = $_GET['u'];
		} else {
			header('Location: signin.php');
			die();
		}
	}

	$user_data = User::getUserByUsername($user);
?>

<div id="navigation">
	<div id="navigation-inner" class="container">
		<div id="logo"><a href="http://localhost/tweb2012/">Phototo</a></div>
		<div class="right">
			<?php include('template/inner_navigation.php') ?>
		</div>

		<div class="clear"></div>
	</div>
</div>

<div id="container">
	<div id="sidebar">
		<div id="user">
			<div class="left" id="avatar">
				<img src="<?php echo User::getAvatarByUsername($user) ?>" alt="" id="avatar">
			</div>
			<div class="left username">
				<p>
					<h2><?php echo $user_data['username']; ?></h2>
				</p>
				<?php if($canDisplayFollow) : ?>
				<p>
					<a href="" class="following">Following</a>
				</p>
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div>

		<?php if($canDisplayUpload) : ?>
		<div id="upload" class="widget">
			<h3>Upload a new picture</h3>
			<form action="app/f/upload_picture.php" enctype="multipart/form-data" method="post">
				<input type="file" name="file" id="upload-input">
				<p>
					<button id="browse-button" class="button-medium-blue">Browse</button>
					<input type="submit" name="upload" value="Upload" class="button-medium">
					<span id="file"></span>
				</p>
				<p>
					<input type="text" name="caption" id="caption" class="input-medium" placeholder="Picture caption" required>
				</p>
			</form>
		</div>
		<?php endif; ?>

		<div id="heartbeat" class="widget">
			<h3>Heartbeats</h3>
		</div>
	</div>

	<div id="posts">
		<?php $posts = Post::getPostsByUserId(User::getIdByUsername($user)); ?>

		<?php if($posts != false) foreach ($posts as $post) : ?>
			<div class="post">
				<div class="image-holder" onclick="location.href='<?php echo SITE_ROOT . 'post.php?id=' . $post['id'] ?>';" style="cursor:pointer;">
					<img src="<?php echo UPLOADS_URL . $user . '/' . $post['filename'] ?>" alt="">
				</div>
				<div class="clear"></div>
				<div class="meta">
					<div><?php echo $post['caption']; ?></div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<?php require_once("template/footer.php") ?>