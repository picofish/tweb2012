<?php require_once("template/header.php") ?>

<?php require_once("app/config.php") ?>

<?php require_once("app/lib/Post.php") ?>
<?php require_once("app/lib/User.php") ?>
<?php require_once("app/lib/Session.php") ?>
<?php require_once("app/lib/Comment.php") ?>

<?php
	$canDisplayUpload = false;
	$canDisplayFollow = false;
	if(Session::isUserSignedIn()) {
		if(isset($_GET['id']) && $_GET['id'] != "") {
			// viewing other users post, i'm logged in
			$user = User::getUsernameByPostId($_GET['id']);
			$canDisplayFollow = true;

			if($user == Session::signedInUser()) {
				$canDisplayUpload = true;
				$canDisplayFollow = false;
			}
		}
	} else {
		if(isset($_GET['id']) && $_GET['id'] != "") {
			// viewing other user, i'm NOT logged in
			$user = User::getUsernameByPostId($_GET['id']);
		} else {
			header('Location: signin.php');
			die();
		}
	}

	$user_data = User::getUserByUsername($user);
?>

<div id="navigation">
	<div id="navigation-inner" class="container">
		<div id="logo"><a href="http://localhost/tweb2012/">Phototo</a></div>
		<div class="right">
			<?php include('template/inner_navigation.php') ?>
		</div>

		<div class="clear"></div>
	</div>
</div>

<div id="container">
	<div id="sidebar">
		<div id="user">
			<div class="left" id="avatar">
				<img src="<?php echo User::getAvatarByUsername($user) ?>" alt="" id="avatar">
			</div>
			<div class="left username">
				<p>
					<h2><?php echo $user_data['username']; ?></h2>
				</p>
				<?php if($canDisplayFollow) : ?>
				<p>
					<?php if(User::isFollowing(User::getIdByUsername(Session::signedInUser()), User::getIdByUsername($user))) : ?>
						<span class="following">Following</span>
					<?php else : ?>
						<span class="notfollowing" id="follow-button">Follow</span>
						<form><input type="hidden" id="victim" value="<?php echo User::getIdByUsername($user) ?>"></form>
					<?php endif; ?>
				</p>
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div>

		<?php if($canDisplayUpload) : ?>
		<div id="upload" class="widget">
			<h3>Upload a new picture</h3>
			<form action="app/f/upload_picture.php" enctype="multipart/form-data" method="post">
				<input type="file" name="file" id="upload-input">
				<p>
					<button id="browse-button" class="button-medium-blue">Browse</button>
					<input type="submit" name="upload" value="Upload" class="button-medium">
					<span id="file"></span>
				</p>
				<p>
					<input type="text" name="caption" id="caption" class="input-medium" placeholder="Picture caption" required>
				</p>
			</form>
		</div>
		<?php endif; ?>

		<div id="heartbeat" class="widget">
			<h3>Heartbeats</h3>
		</div>
	</div>

	<div id="posts">
		<?php $post = Post::getPostById( $_GET['id'] ); ?>
		<?php if($post != false) : ?>
		<h2 class="post-title"><?php echo $post['caption'] ?></h2>
		<div class="post image-holder">
			<img src="<?php echo UPLOADS_URL . $user . '/' . $post['filename'] ?>" alt="">
		</div>

		<div id="comments">
			<?php if(Session::isUserSignedIn()) : ?>
			<form action="app/f/create_comment.php" method="post" id="comments-form">
				<input type="hidden" value="<?php echo $_GET['id'] ?>" name="post_id">
				<p>
					<textarea name="comment_data" class="input"></textarea>
				</p>
				<p id="author">
					<img src="<?php echo User::getAvatarByUsername(Session::signedInUser()) ?>" alt="" class="avatar">
					<span class="username">
						<?php echo Session::signedInUser() ?>
					</span>
				</p>
				<p id="comment-button">
					<input type="submit" name="comment" class="button-medium" value="Send">
				</p>
				<div class="clear"></div>
			</form>
			<?php endif; ?>

			<?php
			$comments = Comment::getCommentsByPostId($_GET['id']);
			if($comments) :  ?>
			<div id="comments-list">
				<ul>
					<?php foreach ($comments as $comment) : ?>
					<li>
						<div class="comment">
							<div class="author">
								<div class="user"><?php echo User::getUsernameById($comment['user_id']) ?></div>
								<div class="date"><?php echo $comment['datetime'] ?></div>
								<div class="clear"></div>
							</div>
							<div class="content">
								<div class="avatar">
									<img src="<?php echo User::getAvatarByUsername(User::getUsernameById($comment['user_id'])) ?>" alt="" class="avatar">
								</div>
								<div class="text">
									<?php echo $comment['comment'] ?>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>
	</div>
</div>

<?php require_once("template/footer.php") ?>