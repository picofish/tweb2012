<?php

require_once('DbTool.php');

class Comment {
	private $_post_id;
	private $_user;
	private $_comment;

	private $_db;

	public function __construct($post_id, $user, $comment) {
		$this->_post_id = $post_id;
		$this->_user = $user;
		$this->_comment = $comment;

		$this->_db = DbTool::connect();
	}

	public function save() {
		$sql = "INSERT INTO comments (post_id, user_id, comment, datetime) VALUES (:post_id, :user_id, :comment, NOW())";
		$attr = array(
			'post_id' => $this->_post_id,
			'user_id' => $this->_user,
			'comment' => $this->_comment
		);

		$q = $this->_db->prepare( $sql );
		return $q->execute( $attr );
	}

	public static function getCommentsByPostId( $i ) {
		$db = DbTool::connect();
		$sql = "SELECT * FROM comments WHERE post_id = :id ORDER BY datetime DESC";
		$attr = array(
			'id' => $i,
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		$data = $q->fetchAll();

		if(count($data) > 1)
			return $data;

		return false;
	}
}