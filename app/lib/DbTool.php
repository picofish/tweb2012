<?php

class DbTool {
	static $_pdo;

	private static $_database = "tweb2012";
	private static $_username = "root";
	private static $_password = "";

	// this function creates or restores a previous connection to the database
	public static function connect() {
		if ( ! self::$_pdo ) {
			self::$_pdo = new PDO( 'mysql:dbname=' . self::$_database, self::$_username, self::$_password );
			self::$_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		}

		return self::$_pdo;
	}
}