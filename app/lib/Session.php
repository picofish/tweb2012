<?php

require_once("DbTool.php");

class Session {
	public static function isUserSignedIn() {
		if( isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1 )
			return true;

		return false;
	}

	public static function signedInUser() {
		if( isset($_SESSION['username']) )
			return $_SESSION['username'];

		return false;
	}

	public static function signUserIn($u, $p) {
		$db = DbTool::connect();

		$sql = "SELECT * FROM users WHERE username = :username AND password = :password";
		$attr = array(
			'username' => $u,
			'password' => $p
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		if(count($q->fetch()) > 1) {
			$_SESSION['username'] = $u;
			$_SESSION['logged_in'] = '1';
			return true;
		} else {
			return false;
		}
	}

	public static function signUserOut() {
		unset($_SESSION['username']);
		unset($_SESSION['logged_in']);
	}
}