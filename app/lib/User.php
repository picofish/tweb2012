<?php

require_once('config.php');

require_once('DbTool.php');

class User {
	private $_username;
	private $_email;
	private $_password;

	private $_db;

	public function __construct( $username, $email , $password ) {
		$this->_username = $username;
		$this->_email = $email;
		$this->_password = $password;
		
		$this->_db = DbTool::connect();
	}

	public function save() {
		$sql = "INSERT INTO users (username, email, password) VALUES (:username, :email, :password); INSERT INTO user_data (username) VALUES (:username)";
		$attr = array(
			'username' => $this->_username,
			'email' => $this->_email,
			'password' => $this->_password
		);

		$q = $this->_db->prepare( $sql );
		return $q->execute( $attr );
	}

	public static function getIdByUsername( $u ) {
		$db = DbTool::connect();
		$sql = "SELECT id FROM users WHERE username = :username";
		$attr = array(
			'username' => $u,
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		$data = $q->fetch();

		if(count($data) > 1)
			return $data['id'];

		return false;
	}

	public static function getUsernameById( $i ) {
		$db = DbTool::connect();
		$sql = "SELECT username FROM users WHERE id = :id";
		$attr = array(
			'id' => $i,
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		$data = $q->fetch();

		if(count($data) > 1)
			return $data['username'];

		return false;
	}

	public static function getUserByUsername( $u ) {
		$db = DbTool::connect();
		$sql = "SELECT users.username as username, name, avatar FROM users, user_data WHERE user_data.username = :username AND users.username = :username";
		$attr = array(
			'username' => $u,
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		$data = $q->fetch();

		if(count($data) > 1)
			return $data;

		return false;
	}

	public static function getUsernameByPostId( $i ) {
		$db = DbTool::connect();
		$sql = "SELECT username 
				FROM users, posts
				WHERE users.id = posts.user_id AND posts.id = :id";
		$attr = array(
			'id' => $i,
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		$data = $q->fetch();

		if(count($data) > 1)
			return $data['username'];

		return false;
	}

	public static function getAvatarByUsername( $u ) {
		$db = DbTool::connect();
		$sql = "SELECT avatar FROM user_data WHERE username = :username";
		$attr = array(
			'username' => $u,
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		$data = $q->fetch();

		if(isset($data['avatar'])) {
			return SITE_ROOT . 'uploads/avatars/' . $data['avatar'];
		}

		return SITE_ROOT . 'uploads/avatars/gravatar.jpg';
	}

	public static function followUser($stalker, $victim) {
		$db = DbTool::connect();
		$sql = "INSERT INTO stalking(stalker, victim) VALUES(:stalker, :victim)";
		$attr = array(
			'stalker' => $stalker,
			'victim' => $victim
		);

		$q = $db->prepare( $sql );
		return $q->execute( $attr );
	}

	public static function isFollowing($stalker, $victim) {
		$db = DbTool::connect();
		$sql = "SELECT id FROM stalking WHERE stalker = :stalker AND victim = :victim";
		$attr = array(
			'stalker' => $stalker,
			'victim' => $victim
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		$data = $q->fetch();

		if(isset($data['id'])) {
			return true;
		}

		return false;
	}
}