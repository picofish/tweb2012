<?php

class Validator {
	public static function username( $u ) {
		return preg_match('/^\w{2,32}$/', $u);
	}

	public static function email( $e ) {
		return filter_var($e, FILTER_VALIDATE_EMAIL);
	}
}