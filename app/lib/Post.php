<?php

require_once('DbTool.php');

class Post {
	private $_userId;
	private $_filename;
	private $_caption;

	private $_db;

	public function __construct( $userId, $filename, $caption ) {
		$this->_userId = $userId;
		$this->_filename = $filename;
		$this->_caption = $caption;
		
		$this->_db = DbTool::connect();
	}

	public function save() {
		$sql = "INSERT INTO posts (user_id, filename, caption, date) VALUES (:user_id, :filename, :caption, NOW())";
		$attr = array(
			'user_id' => $this->_userId,
			'filename' => $this->_filename,
			'caption' => $this->_caption,
		);

		$q = $this->_db->prepare( $sql );
		return $q->execute( $attr );
	}

	public static function getPostsByUserId( $i ) {
		$db = DbTool::connect();
		$sql = "SELECT id, filename, caption FROM posts WHERE user_id = :user_id";
		$attr = array(
			'user_id' => $i,
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		return $q->fetchAll();
	}

	public static function getPostById( $i ) {
		$db = DbTool::connect();
		$sql = "SELECT filename, caption FROM posts WHERE id = :id";
		$attr = array(
			'id' => $i,
		);

		$q = $db->prepare( $sql );
		$q->execute( $attr );

		return $q->fetchAll()[0];
	}
}