<?php
session_start();

if( isset($_POST['upload']) ) {
	require_once("../config.php");
	require_once("../lib/Session.php");
	require_once("../lib/User.php");
	require_once("../lib/Post.php");

	$filename = $_FILES['file']['name'];
	$tempname = $_FILES['file']['tmp_name'];

	if(Session::isUserSignedIn() && move_uploaded_file($tempname, UPLOADS_FOLDER . Session::signedInUser() . '/' . $filename)) {
		$post = new Post(User::getIdByUsername(Session::signedInUser()), $filename, $_POST['caption']);
		$post->save();

		header('Location: ' . SITE_ROOT . 'user.php');
	}
}