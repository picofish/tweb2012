<?php

if( isset($_POST['signup']) ) {
	require_once("../config.php");
	require_once("../lib/User.php");
	require_once("../lib/Validator.php");

	if( Validator::username($_POST['username']) && Validator::email($_POST['email']) ) {
		$user = new User(
			$_POST['username'],
			$_POST['email'],
			$_POST['password']
		);

		if( $user->save() ) {
			mkdir(UPLOADS_FOLDER . $_POST['username']);
			mkdir(THUMBNAILS_FOLDER . $_POST['username']);
			header('Location: ' . SITE_ROOT . 'signup.php?m=success');
		} else {
			header('Location: ' . SITE_ROOT . 'signup.php?m=error');
		}
	} else {
		header('Location: ' . SITE_ROOT . 'signup.php?m=error');
	}
}