<?php
session_start();

if( isset($_POST['signin']) ) {
	require_once("../config.php");
	require_once("../lib/Session.php");
	require_once("../lib/Validator.php");

	if( Validator::username($_POST['username']) ) {
		if( Session::signUserIn($_POST['username'], $_POST['password']) ) {
			header('Location: ' . SITE_ROOT . 'user.php');
		} else {
			header('Location: ' . SITE_ROOT . 'signin.php?m=error');
		}
	}
}