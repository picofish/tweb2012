<?php
session_start();

require_once("../config.php");
require_once("../lib/Session.php");

Session::signUserOut();

session_destroy();

header('Location: ' . SITE_ROOT );