<?php
session_start();

require_once("../lib/User.php");
require_once("../lib/Session.php");

if(User::followUser(User::getIdByUsername(Session::signedInUser()), $_POST['u']))
	echo 'true';
else
	echo 'false';