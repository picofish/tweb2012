<?php
session_start();

if( isset($_POST['comment']) ) {
	require_once("../config.php");

	require_once("../lib/Session.php");
	require_once("../lib/User.php");
	require_once("../lib/Comment.php");

	$post_id = $_POST['post_id'];
	$user_id = User::getIdByUsername(Session::signedInUser());
	$comment = $_POST['comment_data'];

	$comment = new Comment($post_id, $user_id, $comment);

	if($comment->save()) {
		header('Location: ' . SITE_ROOT . 'post.php?id=' . $post_id);
		die();
	} else {
		header('Location: ' . SITE_ROOT . 'post.php?m=error&id=' . $post_id);
		die();
	}
}