<?php

define('SERVER_ROOT', dirname(dirname(__FILE__)));
define('SITE_ROOT', 'http://localhost/tweb2012/');

define('UPLOADS_FOLDER', SERVER_ROOT . '\\uploads\\big\\');
define('THUMBNAILS_FOLDER', SERVER_ROOT . '\\uploads\\thumbnails\\');

define('UPLOADS_URL', 'uploads/big/');