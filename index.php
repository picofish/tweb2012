<?php require_once("template/header.php") ?>

<div id="background-holder">
	<div id="container-frontpage">
		<div id="navigation-frontpage">
			<div id="logo">Phototo</div>
			<div class="right">
				<a href="#">About</a>

				<?php if( Session::isUserSignedIn() ) : ?>
					<a href="http://localhost/tweb2012/user.php">Hello, <?php echo Session::signedInUser() ?></a>
				<?php else : ?>
					<a href="http://localhost/tweb2012/signin.php">Sign in</a>
				<?php endif; ?>
			</div>

			<div class="clear"></div>
		</div>
		<div id="message">
			<h2>Share your best pics</h2>
			<p>Discover, share and talk about inspiring photographs</p>

			<div class="right">
				<a href="http://localhost/tweb2012/signup.php" class="button-front">Join</a>
			</div>
		</div>
	</div>
</div>

</body>
</html>