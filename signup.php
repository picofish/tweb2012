<?php require_once("template/header.php") ?>

<?php if( Session::isUserSignedIn() ) header('Location: user.php') ?>

<div id="navigation">
	<div id="navigation-inner" class="container">
		<div id="logo"><a href="http://localhost/tweb2012/">Phototo</a></div>
		<div class="right">
			<?php include('template/inner_navigation.php') ?>
		</div>

		<div class="clear"></div>
	</div>
</div>

<div id="container">
	<?php if( isset($_GET['m']) && $_GET['m'] == 'success') : ?>
		<div class="notification">
			Your account has been created and must be approved.
		</div>
	<?php endif; if( isset($_GET['m']) && $_GET['m'] == 'error') : ?>
		<div class="error">
			Your account cannot be created.
		</div>
	<?php endif; ?>

	<div id="content">
		<h2>Create a new account</h2>
		<form action="app/f/create_user.php" id="signup" method="post">
			<p>
				<label for="username">Username</label>
				<input type="text" name="username" class="input">
			</p>
			<p>
				<label for="email">Email</label>
				<input type="text" name="email" class="input">
			</p>
			<p>
				<label for="password">Password</label>
				<input type="password" name="password" class="input">
			</p>
			<p>
				<input type="submit" name="signup" value="Sign Up" class="button">
			</p>
		</form>
	</div>
</div>

<?php require_once("template/footer.php") ?>