<?php 
	session_start();
	require_once('app/lib/Session.php');
?>
<!doctype html>
<html>
	<head>
		<link rel="stylesheet" href="assets/css/reset.css">
		<link rel="stylesheet" href="assets/css/default.css">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arvo:400|Open+Sans:400,700">

		<script src="assets/js/jquery-1.8.3.min.js"></script>
		<script src="assets/js/app.js"></script>
	</head>

	<body>